---
title: A propos
date: 18/07/2022/11:59:34
draft: false
description: Présentation
tags:
  - a propos
  - projets
  - présentation
  - langages
categories:
  - Projets
  - Présentation
  - Langages
lastmod: 18/07/2022/11:59:58
slug: propos
---

# Qui suis-je ?

Je m'appelle Alexandre. J'ai fais de ma passion du numérique une réalité grâce à la découverte de la programmation.
J'ai fais un titre professionnel dans le développement web et web mobile entre 2021 et 2022.
J'aspire à devenir un développeur junior en entreprise orienté dans le JavaScript.

## Quels langages et quel framework je connais ?

J'utilise principalement du JavaScript. Je connais le HTML/CSS, PHP/SQL et je me suis intéressé à Python.
Pour le framework j'utilise React.js (même si c'est plus une librairie qu'un framework). J'espère en savoir assez pour enfin m'attaquer à Next.js. Je suis en apprentissage de TypeScript afin de sécurisé mon code.

## Mes projets futurs.

Actuellement je suis en train de réfléchir à faire une sorte de réseau social qui comporterait un espace discussion, un espace où il y aurait des mini-jeux (comme des morpions, pierre feuille ciseaux, ...), et un espace où il y aurait les actualités qui seraient mis à disposition selon le choix de l'utilisateur.

#### Mon Portfolio

Vous pouvez retrouver mon portfolio sur le lien suivant: https://portfolio-alexandre-benoit-fontaine.netlify.app
