---
title: "Le gros projet "
description: Je te présente l'idée de projet que j'aimerai réaliser.
date: 18/07/2022/02:57:36
preview: ""
draft: ""
tags:
  - projet
  - présentation
categories:
  - Projet
lastmod: 18/07/2022/03:18:14
slug: le-gros-projet
---

# Idée du projet

Ce projet est inspiré d'une plateforme que j'avais administré avec des collègue durant mon service civique. On devait faire vivre la plateforme. La plateforme avait un espace de discussion, un espace avec des mini-jeux et un espace avec les actualités.

# Mise en place du projet

Pour mettre en place ce projet je pense utiliser React.js pour le front et sûrement Node.js pour le back avec Express.js. La BDD sera sur phpMyAdmin pour pouvoir schématiser les relations.

# Maquettage du projet

Pour le maquettage je vais utiliser sur Figma. Je pense m'inspirer de réseaux sociaux connu pour l'UX/UI et dribbble pour rechercher d'autres inspirations.
